/*
 * stdlib_allocator.c
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/domain/common/allocator_interface.h"

/* Depends */
#include <stdlib.h>
#include "pico-kit-reactive/domain/error_model.h"

r__error r__allocator_allocate(r__allocator_type allocator, size_t size, void ** data)
{
	void * memory;

	(void)allocator;

	if (data == NULL) {
		return R__ERROR_NULL_ARGUMENT;
	}
	if (size == 0) {
		*data = NULL;
		return R__ERROR_NONE;
	}
	memory = malloc(size);
	if (memory == NULL) {
		goto FAIL_ALLOCATE_MEMORY;
	}
	*data = memory;
	return R__ERROR_NONE;
FAIL_ALLOCATE_MEMORY:
	*data = NULL;
	return R__ERROR_NO_MEMORY;
}

r__error r__allocator_deallocate(r__allocator_type allocator, void * memory)
{
	(void)allocator;

	if (memory == NULL) {
		return R__ERROR_NONE;
	}
	free(memory);
	return R__ERROR_NONE;
}
