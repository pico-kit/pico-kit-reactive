
# Dependency inversion

## Problem

Component A depends on Component B, but it should not include it.

```plantuml
component "Component A" <<Component>> {
  file component_a.c as source_a
  file component_a.h as interface_a
  interface_a <.. source_a : realizes
}

component "Component B" <<Component>> {
  file component_b.c as source_b
  file component_b.h as interface_b
  source_b ..> interface_b : realizes
}

source_a -> interface_b
```

```plantuml
component "Component A" <<Component>> {
  file component_a.c as source_a
  file component_a.h as interface_a
  file component_a_dependency_interface.h as dependency_interface
  source_a -> dependency_interface : depends
  interface_a <.. source_a : realizes
}

component "Component B" <<Component>> {
  file component_b.c as source_b
  file component_b.h as interface_b
  source_b ..> interface_b : realizes
}

component "Component A dependency adapter" <<Component>> {
   file component_a_dependency_adapter.c as adapter
}

dependency_interface <. adapter : realizes
adapter -> interface_b : depends
```

# Domain

```plantuml
component "Agent" <<Component>> as agent
component "Event" <<Component>> as event
component "Generic Queue" <<Component>> as generic_queue
component "Event Queue" <<Component>> as event_queue
component "Queue" <<Component>> as agent_queue
component "State Machine" <<Component>> as sm
component "Timer" <<Component>> as timer
component "Network" <<Component>> as network
component "Network Node" <<Component>> as network_node
component "Channel" <<Component>> as channel
component "Configuration" <<Component>> as configuration
component "Generic Linked List" <<Component>>
component "Platform" <<Component>>

interface scheduler
interface allocator
interface arithmetic

agent --> sm
agent --> event
agent --> event_queue
agent --> network_node
agent --> network

event_queue --> event
event_queue --> generic_queue

sm --> event
sm --> configuration

timer --> agent
timer --> event

channel --> agent
channel --> event

agent_queue --> agent
agent_queue --> event_queue

network --> network_node
network --> scheduler
```