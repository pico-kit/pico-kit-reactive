/*
 * allocator_service.h
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_COMMON_ALLOCATOR_ALLOCATOR_SERVICE_H_
#define REACTIVE_FEATURE_COMMON_ALLOCATOR_ALLOCATOR_SERVICE_H_

#include <stdint.h>
#include <stddef.h>
#include "pico-kit-reactive/domain/error.h"

enum r__allocator_type
{
	R__ALLOCATOR_TYPE_EVENT,
	R__ALLOCATOR_TYPE_EVENT_DATA,
	R__ALLOCATOR_TYPE_QUEUE,
	R__ALLOCATOR_TYPE_QUEUE_DATA,
	R__ALLOCATOR_TYPE_AGENT,
	R__ALLOCATOR_TYPE_AGENT_DATA,
	R__ALLOCATOR_TYPE_TIMER,
	R__ALLOCATOR_TYPE_NETWORK,
	R__ALLOCATOR_TYPE_NETWORK_SCHEDULER,
	R__ALLOCATOR_TYPE_NETWORK_TASK,
};

typedef uint32_t r__allocator_type;

r__error r__allocator_allocate(r__allocator_type allocator, size_t size, void ** data);
r__error r__allocator_deallocate(r__allocator_type allocator, void * mem);

#endif /* REACTIVE_FEATURE_COMMON_ALLOCATOR_ALLOCATOR_SERVICE_H_ */
