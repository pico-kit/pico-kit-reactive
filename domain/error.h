/*
 * error.h
 *
 *  Created on: Oct 16, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_ERROR_H_
#define REACTIVE_DOMAIN_ERROR_H_

#include <stdint.h>

typedef uint32_t r__error;

#endif /* REACTIVE_DOMAIN_ERROR_H_ */
