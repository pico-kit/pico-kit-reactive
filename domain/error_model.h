/*
 * error_model.h
 *
 *  Created on: Oct 10, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_ERROR_MODEL_H_
#define DOMAIN_ERROR_MODEL_H_

#include "pico-kit-reactive/domain/error.h"

enum r__error
{
	R__ERROR_NONE,
	R__ERROR_NULL_ARGUMENT,
	R__ERROR_INVALID_ARGUMENT,
	R__ERROR_INVALID_OBJECT,
	R__ERROR_NO_MEMORY,
	R__ERROR_INVALID_SM_ACTION,
	R__ERROR_INVALID_SM_ENTER,
	R__ERROR_NO_SPACE,
	R__ERROR_NO_ENTITY,
	R__ERROR_SCHEDULER_FAILURE,
};

#endif /* DOMAIN_ERROR_MODEL_H_ */
