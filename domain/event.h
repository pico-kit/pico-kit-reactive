/*
 * event.h
 *
 *  Created on: Oct 16, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_EVENT_H_
#define REACTIVE_DOMAIN_EVENT_H_

#include <stdint.h>

typedef int32_t r__event_id;

typedef struct r__event r__event;

#endif /* REACTIVE_DOMAIN_EVENT_H_ */
