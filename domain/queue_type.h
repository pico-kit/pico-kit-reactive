/*
 * queue_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_QUEUE_TYPE_H_
#define DOMAIN_QUEUE_TYPE_H_

#include <stdint.h>
#include "pico-kit-reactive/domain/agent.h"
#include "pico-kit-reactive/domain/queue.h"
#include "pico-kit-reactive/domain/common/generic_queue/generic_queue_model.h"

struct r__queue
{
	r__generic_queue p__queue;
	r__agent * agent;
};

#endif /* DOMAIN_QUEUE_TYPE_H_ */
