/*
 * agent_create.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/agent/agent_create.h"

/* Depends */
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/agent_model.h"
#include "pico-kit-reactive/domain/network_model.h"
#include "pico-kit-reactive/domain/common/allocator_interface.h"

r__error r__agent_create(
		r__network * network,
		r__network_task_priority network_task_priority,
		r__sm_state * sm_initial_state,
		size_t sm_data_size,
		size_t queue_storage_elements,
		r__agent ** agent)
{
	r__error error;
	r__agent * l_agent;
	void * agent_mem;
	void * sm_data;
	void * queue_storage;

	/* Validate arguments */
	if ((network == NULL) || (agent == NULL) || (sm_initial_state == NULL)) {
		return R__ERROR_NULL_ARGUMENT;
	}
	if (queue_storage_elements == 0) {
		return R__ERROR_INVALID_ARGUMENT;
	}

	/* Allocate agent instance and variable sized data */
	error = r__allocator_allocate(
			R__ALLOCATOR_TYPE_AGENT,
			sizeof(r__agent),
			&agent_mem);
	if (error) {
		goto FAIL_ALLOCATE_AGENT;
	}
	error = r__allocator_allocate(
			R__ALLOCATOR_TYPE_AGENT_DATA,
			sm_data_size,
			&sm_data);
	if (error) {
		goto FAIL_ALLOCATE_SM_DATA;
	}
	error = r__allocator_allocate(
			R__ALLOCATOR_TYPE_AGENT_DATA,
			sizeof(r__event *) * queue_storage_elements,
			&queue_storage);
	if (error) {
		goto FAIL_ALLOCATE_QUEUE_STORAGE;
	}

	l_agent = agent_mem;

	/* Construct agent instance */
	r__agent_model_ctor_dynamic(
			l_agent,
			network,
			sm_initial_state,
			sm_data,
			queue_storage,
			queue_storage_elements);

	/* Create task that will execute this agent */
	error = r__network_model_proxy_create_task(
			&l_agent->network_proxy,
			network_task_priority);
	if (error) {
		goto FAIL_CREATE_TASK;
	}

	*agent = l_agent;
	return R__ERROR_NONE;
	/* Error handling */
FAIL_CREATE_TASK:
	r__agent_model_dtor(l_agent);
	(void)r__allocator_deallocate(R__ALLOCATOR_TYPE_AGENT_DATA, queue_storage);
FAIL_ALLOCATE_QUEUE_STORAGE:
	(void)r__allocator_deallocate(R__ALLOCATOR_TYPE_AGENT_DATA, sm_data);
FAIL_ALLOCATE_SM_DATA:
	(void)r__allocator_deallocate(R__ALLOCATOR_TYPE_AGENT, agent_mem);
FAIL_ALLOCATE_AGENT:
	*agent = NULL;
	return error;
}
