/*
 * agent_create.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_AGENT_AGENT_CREATE_H_
#define FEATURE_AGENT_AGENT_CREATE_H_

#include <stddef.h>
#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/sm.h"
#include "pico-kit-reactive/domain/agent.h"
#include "pico-kit-reactive/domain/network.h"

r__error r__agent_create(
		r__network * network,
		r__network_task_priority network_task_priority,
		r__sm_state * sm_initial_state,
		size_t sm_data_size,
		size_t queue_storage_elements,
		r__agent ** agent);

#endif /* FEATURE_AGENT_AGENT_CREATE_H_ */
