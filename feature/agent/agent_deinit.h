/*
 * agent_deinit.h
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_AGENT_AGENT_DEINIT_H_
#define REACTIVE_FEATURE_AGENT_AGENT_DEINIT_H_

#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/agent.h"

r__error r__agent_deinit(r__agent * agent);

#endif /* REACTIVE_FEATURE_AGENT_AGENT_DEINIT_H_ */
