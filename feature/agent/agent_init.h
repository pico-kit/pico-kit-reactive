/*
 * agent_init.h
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_AGENT_AGENT_INIT_H_
#define REACTIVE_FEATURE_AGENT_AGENT_INIT_H_

#include <stddef.h>
#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/sm.h"
#include "pico-kit-reactive/domain/agent.h"
#include "pico-kit-reactive/domain/network.h"

r__error r__agent_init(
		r__agent * agent,
		r__network * network,
		r__network_task * network_task,
		r__network_task_priority network_task_priority,
		r__sm_state * sm_initial_state,
		void * sm_data,
		size_t sm_data_size,
		void * queue_storage,
		size_t queue_storage_elements);

#endif /* REACTIVE_FEATURE_AGENT_AGENT_INIT_H_ */
