/*
 * event_delete.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/event/event_delete.h"

/* Depends */
#include <stddef.h>
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/event_model.h"

r__error r__event_delete(const r__event * event)
{
	r__error error;

	/* Validate arguments */
	if (event == NULL) {
		return R__ERROR_NULL_ARGUMENT;
	}

	/* Delete event */
	error = r__event_model_delete(event);
	if (error) {
		goto FAIL_DELETE_EVENT;
	}
	return R__ERROR_NONE;

	/* Error handling */
FAIL_DELETE_EVENT:
	return error;
}
