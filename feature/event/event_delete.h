/*
 * event_delete.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef FEATURE_EVENT_DELETE_H_
#define FEATURE_EVENT_DELETE_H_

#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/event.h"

r__error r__event_delete(const r__event * event);

#endif /* FEATURE_EVENT_DELETE_H_ */
