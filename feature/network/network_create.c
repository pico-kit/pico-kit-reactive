/*
 * network_create.c
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/network/network_create.h"

/* Depends */
#include <stddef.h>
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/network_model.h"
#include "pico-kit-reactive/domain/common/allocator_interface.h"

r__error r__network_create(r__network_scheduler * network_scheduler, r__network ** network)
{
	r__error error;
	void * l_network;

	/* Validate arguments */
	if ((network_scheduler == NULL) || (network == NULL)) {
		return R__ERROR_NULL_ARGUMENT;
	}

	/* Allocate network instance */
	error = r__allocator_allocate(R__ALLOCATOR_TYPE_NETWORK, sizeof(r__network), &l_network);
	if (error) {
		goto FAIL_ALLOCATE_NETWORK;
	}

	/* Construct network instance */
	r__network_model_ctor(l_network, network_scheduler);
	*network = l_network;
	return R__ERROR_NONE;

	/* Error handling */
FAIL_ALLOCATE_NETWORK:
	return error;
}
