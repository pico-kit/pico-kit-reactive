/*
 * network_create.h
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_NETWORK_NETWORK_CREATE_H_
#define REACTIVE_FEATURE_NETWORK_NETWORK_CREATE_H_

#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/network.h"

r__error r__network_create(r__network_scheduler * network_scheduler, r__network ** network);

#endif /* REACTIVE_FEATURE_NETWORK_NETWORK_CREATE_H_ */
