/*
 * network_start.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_NETWORK_NETWORK_START_H_
#define REACTIVE_FEATURE_NETWORK_NETWORK_START_H_

#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/network.h"

r__error r__network_start(r__network * network);

#endif /* REACTIVE_FEATURE_NETWORK_NETWORK_START_H_ */
