/*
 * queue_create.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/queue/queue_create.h"

/* Depends */
#include <stddef.h>
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/queue_model.h"
#include "pico-kit-reactive/domain/agent_model.h"
#include "pico-kit-reactive/domain/common/allocator_interface.h"

r__error r__queue_create(r__sm * sm, size_t n_entries, r__queue ** queue)
{
	r__error error;
	void * storage;
	void * l_queue;

	/* Validate arguments */
	if ((sm == NULL) || (queue == NULL)) {
		return R__ERROR_NULL_ARGUMENT;
	}
	if (n_entries == 0u) {
		return R__ERROR_INVALID_ARGUMENT;
	}

	/* Allocate queue instance and variable sized data */
	error = r__allocator_allocate(R__ALLOCATOR_TYPE_QUEUE, sizeof(r__queue), &l_queue);
	if (error) {
		goto FAIL_ALLOCATE_QUEUE;
	}
	error = r__allocator_allocate(R__ALLOCATOR_TYPE_QUEUE_DATA, sizeof(r__event *) * n_entries, &storage);
	if (error) {
		goto FAIL_ALLOCATE_STORAGE;
	}

	/* Construct queue instance */
	r__queue_model_ctor(l_queue, r__agent_model_decompose(sm), storage, n_entries);
	*queue = l_queue;
	return R__ERROR_NONE;

	/* Error handling */
FAIL_ALLOCATE_STORAGE:
	(void)r__allocator_deallocate(R__ALLOCATOR_TYPE_QUEUE, l_queue);
FAIL_ALLOCATE_QUEUE:
	*queue = NULL;
	return error;
}
