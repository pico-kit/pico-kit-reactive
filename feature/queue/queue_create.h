/*
 * queue_create.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef FEATURE_QUEUE_QUEUE_CREATE_H_
#define FEATURE_QUEUE_QUEUE_CREATE_H_

#include <stddef.h>
#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/sm.h"
#include "pico-kit-reactive/domain/queue.h"

r__error r__queue_create(r__sm * sm, size_t n_entries, r__queue ** queue);

#endif /* FEATURE_QUEUE_QUEUE_CREATE_H_ */
