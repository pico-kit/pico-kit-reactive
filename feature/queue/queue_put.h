/*
 * queue_put.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_QUEUE_QUEUE_PUT_H_
#define FEATURE_QUEUE_QUEUE_PUT_H_

#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/event.h"
#include "pico-kit-reactive/domain/queue.h"

r__error r__queue_put_fifo(r__queue * queue, const r__event * event);
r__error r__queue_put_lifo(r__queue * queue, const r__event * event);

#endif /* FEATURE_QUEUE_QUEUE_PUT_H_ */
