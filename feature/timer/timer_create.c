/*
 * timer_create.c
 *
 *  Created on: Oct 13, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/timer/timer_create.h"

/* Depends */
#include <stddef.h>
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/event_model.h"
#include "pico-kit-reactive/domain/agent_model.h"
#include "pico-kit-reactive/domain/timer_model.h"
#include "pico-kit-reactive/domain/common/allocator_interface.h"

r__error r__timer_create(r__sm * sm, r__event_id event_id, r__timer ** timer)
{
	r__agent * agent;
	r__error error;
	void * l_timer;

	if ((sm == NULL) || (timer == NULL)) {
		return R__ERROR_NULL_ARGUMENT;
	}
	error = r__allocator_allocate(R__ALLOCATOR_TYPE_TIMER, sizeof(r__timer), &l_timer);
	if (error) {
		goto FAIL_ALLOCATE_TIMER;
	}
	agent = r__agent_model_decompose(sm);
	r__timer_model_ctor_dynamic(l_timer, event_id, agent);
	*timer = l_timer;
	return R__ERROR_NONE;
FAIL_ALLOCATE_TIMER:
	*timer = NULL;
	return error;
}
