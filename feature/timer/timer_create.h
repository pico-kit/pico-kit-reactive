/*
 * timer_create.h
 *
 *  Created on: Oct 13, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_TIMER_TIMER_CREATE_H_
#define REACTIVE_FEATURE_TIMER_TIMER_CREATE_H_

#include "pico-kit-reactive/domain/sm.h"
#include "pico-kit-reactive/domain/error.h"
#include "pico-kit-reactive/domain/event.h"
#include "pico-kit-reactive/domain/timer.h"

r__error r__timer_create(r__sm * sm, r__event_id event_id, r__timer ** timer);

#endif /* REACTIVE_FEATURE_TIMER_TIMER_CREATE_H_ */
