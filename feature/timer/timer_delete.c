/*
 * timer_delete.c
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/timer/timer_delete.h"

/* Depends */
#include <stddef.h>
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/agent_model.h"
#include "pico-kit-reactive/domain/timer_model.h"
#include "pico-kit-reactive/domain/network_model.h"
#include "pico-kit-reactive/domain/common/allocator_interface.h"

r__error r__timer_delete(r__timer * timer)
{
	r__error error;
	r__network * network;

	/* Validate argument */
	if (timer == NULL) {
		return R__ERROR_NULL_ARGUMENT;
	}

	network = r__network_model_proxy_get_network(
			r__agent_model_get_network_proxy(timer->agent));

	error = r__network_model_lock(network);
	if (error) {
		goto FAIL_NETWORK_LOCK;
	}
	r__timer_model_cancel(timer);
	error = r__network_model_lock(network);
	if (error) {
		goto FAIL_NETWORK_UNLOCK;
	}
	r__timer_model_dtor(timer);
	error = r__allocator_deallocate(R__ALLOCATOR_TYPE_TIMER, timer);
	if (error) {
		goto FAIL_DEALLOCATE_TIMER;
	}
	return R__ERROR_NONE;
FAIL_DEALLOCATE_TIMER:
FAIL_NETWORK_UNLOCK:
FAIL_NETWORK_LOCK:
	return error;
}
