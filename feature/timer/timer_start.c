/*
 * timer_start.c
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

/* Implements */
#include "pico-kit-reactive/feature/timer/timer_start.h"

/* Depends */
#include <stddef.h>
#include "pico-kit-reactive/domain/error_model.h"
#include "pico-kit-reactive/domain/timer_model.h"
#include "pico-kit-reactive/domain/agent_model.h"

r__error r__timer_start_after(r__timer * timer, uint32_t timeout_ms)
{
	if (timer == NULL) {
		return R__ERROR_NULL_ARGUMENT;
	}
	if (timeout_ms == 0u) {
		return r__agent_model_send_back(timer->agent, &timer->event);
	}
	return R__ERROR_NONE;
}

r__error r__timer_start_every(r__timer * timer, uint32_t timeout_ms)
{
	if (timer == NULL) {
		return R__ERROR_NULL_ARGUMENT;
	}
	if (timeout_ms == 0u) {
		return r__agent_model_send_back(timer->agent, &timer->event);
	}
	return R__ERROR_NONE;
}
