# manifest.mk

# General
F_MODULES_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive

# Domain and Feature
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/domain
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/domain/common
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/domain/common/generic_list
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/domain/common/generic_queue
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/domain/common/generic_timer
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/feature/agent
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/feature/event
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/feature/network
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/feature/queue
F_SRC_DIRS_$(CONFIG_REACTIVE_ENABLE) += pico-kit-reactive/feature/timer
F_INC_DIRS_$(CONFIG_REACTIVE_ENABLE) += .

# Allocator adapter
F_SRC_DIRS_$(CONFIG_REACTIVE_ALLOCATOR_ADAPTER_STD) += pico-kit-reactive/adapter/allocator/std

# Scheduler adapter
F_SRC_DIRS_$(CONFIG_REACTIVE_SCHEDULER_ADAPTER_HARMONY) += pico-kit-reactive/adapter/scheduler/harmony
F_DEPENDENCIES_$(CONFIG_REACTIVE_SCHEDULER_ADAPTER_HARMONY) += pico-kit-harmony
